package com.bookmgmt.tatsuya.bookmanagement.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bookmgmt.tatsuya.bookmanagement.R;
import com.bookmgmt.tatsuya.bookmanagement.adapter.BookAdapter;
import com.bookmgmt.tatsuya.bookmanagement.dao.BookDAO;
import com.bookmgmt.tatsuya.bookmanagement.dao.BookDAOImpl;
import com.bookmgmt.tatsuya.bookmanagement.model.Account;
import com.bookmgmt.tatsuya.bookmanagement.model.Book;
import com.bookmgmt.tatsuya.bookmanagement.utils.SQLiteConnector;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MainActivity extends AppCompatActivity {
    private TextView welcomeTextView;
    private ListView listViewProduct;
    private Button creationBtn;
    private Spinner spinner;
    private AutoCompleteTextView completeTextView;
    private Button btnSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mapViewsComponents();
        getDataFromIntent();

        BookDAO bookDAO = new BookDAOImpl(new SQLiteConnector(this));
//        Book b1 = new Book("Book1", "Java 9 Programming", "Technique", "Peter Verhas", "https://d255esdrn735hr.cloudfront.net/sites/default/files/imagecache/ppv4_main_book_cover/9781786468284.png");
//        Book b3 = new Book("Book3", "Hong Lau Mong", "Novel", "Tao Tuyet Can", "https://www.sachkhaitam.com/Data/Sites/1/Product/1444/hong-lau-mong.jpg");
//        Book b2 = new Book("Book2", "Python in Practice", "Technique", "Mark Summerfield", "http://twimgs.com/ddj/galleries/44/PythonInPractice_full.jpg");
//        Book b4 = new Book("Book4", "Tay Du Ky", "Novel", "Ngo Thua An", "http://isach.info/images/story/cover/tay_du_ky__ngo_thua_an.jpg");
//        Book b5 = new Book("Book5", "Khong Gia Dinh", "Children", "Hector Malot", "https://vcdn.tikicdn.com/cache/550x550/media/catalog/product/k/h/khong-gia-dinh_5.jpg");
//
//        bookDAO.createBook(b1);
//        bookDAO.createBook(b2);
//        bookDAO.createBook(b3);
//        bookDAO.createBook(b4);
//        bookDAO.createBook(b5);

        List<Book> bookList = bookList = bookDAO.findAll();

        String[] names = getNameList(bookList);
        List<String> types = getTypeList(bookList);
        completeTextView.setThreshold(1);
        setSpinnerValue(types);
        setCompleteTextValue(names);
        setBookAdapterValue(bookList);

        this.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            private int checked = 0;

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (++checked > 1) {
                    if (types.get(i).equals("None")) {
                        BookDAO dao = new BookDAOImpl(new SQLiteConnector(MainActivity.this));
                        List<Book> result = dao.findAll();
                        setBookAdapterValue(result);
                    }
                    else{
                        String type = types.get(i);
                        BookDAO dao = new BookDAOImpl(new SQLiteConnector(MainActivity.this));
                        List<Book> result = dao.searchBookByType(type);
                        setBookAdapterValue(result);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        this.creationBtn.setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this, BookCreationActivity.class);
            startActivity(intent);
        });

        this.btnSearch.setOnClickListener(view -> {
            String value = MainActivity.this.completeTextView.getText().toString();
            BookDAO dao = new BookDAOImpl(new SQLiteConnector(MainActivity.this));
            List<Book> result = dao.searchBookByName(value);
            setBookAdapterValue(result);
        });


    }

    private void mapViewsComponents() {
        this.welcomeTextView = super.findViewById(R.id.welcomeTextView);
        this.listViewProduct = super.findViewById(R.id.listViewProduct);
        this.creationBtn = super.findViewById(R.id.creationBtn);
        this.completeTextView = super.findViewById(R.id.searchField);
        this.spinner = super.findViewById(R.id.customSearching);
        this.btnSearch = super.findViewById(R.id.searchBtn);
    }

    public void getDataFromIntent() {
        Intent intent = super.getIntent();
        if (intent != null) {
            Account account = (Account) intent.getSerializableExtra("loggedUser");
            if (account != null) {
                this.welcomeTextView.setText(welcomeTextView.getText() + ", " + account.getUsername());
            }
        }
    }

    private void setSpinnerValue(List<String> list) {
        ArrayAdapter<String> spinnerAdaper = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, list);
        spinnerAdaper.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.spinner.setAdapter(spinnerAdaper);
    }

    private void setCompleteTextValue(String[] src) {
        ArrayAdapter adapterSearching = new ArrayAdapter(this,
                android.R.layout.simple_list_item_1, src);
        completeTextView.setAdapter(adapterSearching);
    }

    private void setBookAdapterValue(List<Book> bookList) {
        BookAdapter bookAdapter
                = new BookAdapter(this, R.layout.activity_book_line, bookList);
        this.listViewProduct.setAdapter(bookAdapter);
    }

    private List<String> getTypeList(List<Book> bookList) {
        Set<String> setTypes = new HashSet<>();
        setTypes.add("None");
        for (Book book : bookList) {
            setTypes.add(book.getType());
        }

        List<String> result = new ArrayList<>();
        result.addAll(setTypes);
        return result;
    }

    private String[] getNameList(List<Book> bookList) {
        List<String> listName = new ArrayList<>();
        for (Book book : bookList) {
            listName.add(book.getBookName());
        }
        return listName.toArray(new String[listName.size()]);
    }
}
