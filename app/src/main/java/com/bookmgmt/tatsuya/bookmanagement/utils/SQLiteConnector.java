package com.bookmgmt.tatsuya.bookmanagement.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by tatsuya on 22/03/2018.
 */

public class SQLiteConnector extends SQLiteOpenHelper {
    public static final String dbName = "bookmgmt";

    private Context context;

    public SQLiteConnector(Context context) {
        super(context, dbName, null, 1);
        Log.d("DBManager", "DBManager: ");
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String sqlCommandBook = "CREATE TABLE book  ("                          +
                                                    "id  TEXT PRIMARY KEY, "    +
                                                    "name TEXT, "               +
                                                    "type TEXT, "               +
                                                    "author TEXT,"              +
                                                    "imageurl TEXT" +
                                                    ")";

        String sqlCommandAccount = "CREATE TABLE account (username TEXT PRIMARY KEY, password TEXT)";

        sqLiteDatabase.execSQL(sqlCommandBook);
        sqLiteDatabase.execSQL(sqlCommandAccount);
        Toast.makeText(context, "Create successflly", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + "book");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + "account");
        onCreate(sqLiteDatabase);
        Toast.makeText(context, "Drop successflly", Toast.LENGTH_SHORT).show();
    }
}
