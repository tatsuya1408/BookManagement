package com.bookmgmt.tatsuya.bookmanagement.model;

import java.io.Serializable;

/**
 * Created by tatsuya on 22/03/2018.
 */

public class Book implements Serializable {
    private String bookID;
    private String bookName;
    private String type;
    private String author;
    private String imageURL;

    public Book() {
    }

    public Book(String bookID, String bookName, String type, String author, String imageURL) {
        this.bookID = bookID;
        this.bookName = bookName;
        this.type = type;
        this.author = author;
        this.imageURL = imageURL;
    }

    public String getBookID() {
        return bookID;
    }

    public void setBookID(String bookID) {
        this.bookID = bookID;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    @Override
    public String toString() {
        return "Book{" +
                "bookID='" + bookID + '\'' +
                ", bookName='" + bookName + '\'' +
                ", type='" + type + '\'' +
                ", author='" + author + '\'' +
                ", imageURL='" + imageURL + '\'' +
                '}';
    }
}
