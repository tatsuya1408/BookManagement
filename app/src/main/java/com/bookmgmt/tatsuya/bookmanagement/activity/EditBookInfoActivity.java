package com.bookmgmt.tatsuya.bookmanagement.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bookmgmt.tatsuya.bookmanagement.R;
import com.bookmgmt.tatsuya.bookmanagement.dao.BookDAO;
import com.bookmgmt.tatsuya.bookmanagement.dao.BookDAOImpl;
import com.bookmgmt.tatsuya.bookmanagement.model.Book;
import com.bookmgmt.tatsuya.bookmanagement.utils.SQLiteConnector;

public class EditBookInfoActivity extends AppCompatActivity {
    private EditText etID;
    private EditText etName;
    private EditText etType;
    private EditText etAuthor;
    private EditText etImageURL;
    private Button btnUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_book_info);
        mapViewComponents();
        Intent intent = super.getIntent();
        Bundle editBookBundle = intent.getBundleExtra("editBookBundle");
        Book b = (Book) editBookBundle.getSerializable("editBook");
        pushDataToEditText(b);

        this.btnUpdate.setOnClickListener(view -> {
            Book updatedBook = new Book();
            updatedBook.setBookID(EditBookInfoActivity.this.etID.getText().toString());
            updatedBook.setBookName(EditBookInfoActivity.this.etName.getText().toString());
            updatedBook.setType(EditBookInfoActivity.this.etType.getText().toString());
            updatedBook.setAuthor(EditBookInfoActivity.this.etAuthor.getText().toString());
            updatedBook.setImageURL(EditBookInfoActivity.this.etImageURL.getText().toString());

            BookDAO bookDAO = new BookDAOImpl(new SQLiteConnector(this));
            bookDAO.editBookInfo(updatedBook);

            Toast.makeText(this, "OK", Toast.LENGTH_SHORT).show();
            Intent refresh = new Intent(this, MainActivity.class);
            super.startActivity(refresh);
            super.finish(); //
        });
    }

    private void pushDataToEditText(Book b) {
        this.etID.setText(b.getBookID());
        this.etName.setText(b.getBookName());
        this.etType.setText(b.getType());
        this.etAuthor.setText(b.getAuthor());
        this.etImageURL.setText(b.getImageURL());
    }

    private void mapViewComponents() {
        this.etID = super.findViewById(R.id.etidbook);
        this.etName = super.findViewById(R.id.etName);
        this.etType = super.findViewById(R.id.etType);
        this.etAuthor = super.findViewById(R.id.etAuthor);
        this.etImageURL = super.findViewById(R.id.etImageURL);
        this.btnUpdate = super.findViewById(R.id.btnUpdate);
    }
}
