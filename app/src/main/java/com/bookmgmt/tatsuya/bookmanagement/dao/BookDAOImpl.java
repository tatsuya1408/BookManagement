package com.bookmgmt.tatsuya.bookmanagement.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteStatement;

import com.bookmgmt.tatsuya.bookmanagement.model.Book;
import com.bookmgmt.tatsuya.bookmanagement.utils.SQLiteConnector;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tatsuya on 22/03/2018.
 */

public class BookDAOImpl implements BookDAO {
    private SQLiteConnector SQLiteConnector;

    public BookDAOImpl(SQLiteConnector SQLiteConnector) {
        this.SQLiteConnector = SQLiteConnector;
    }

    @Override
    public boolean createBook(Book book) throws SQLException {
//        SQLiteDatabase connection = SQLiteConnector.getReadableDatabase();
//        ContentValues values = new ContentValues();
//        values.put("id", book.getBookID());
//        values.put("name", book.getBookName());
//        values.put("type", book.getType());
//        values.put("author", book.getAuthor());
//        values.put("imageurl", book.getImageURL());
//        connection.insert("book", null, values);
//        connection.close();
        SQLiteDatabase connection = SQLiteConnector.getWritableDatabase();
        String sql = "INSERT INTO book VALUES (?,?,?,?,?)";
        SQLiteStatement statement = connection.compileStatement(sql);
        statement.bindString(1,book.getBookID());
        statement.bindString(2,book.getBookName());
        statement.bindString(3,book.getType());
        statement.bindString(4,book.getAuthor());
        statement.bindString(5,book.getImageURL());
        long rowId = statement.executeInsert();
        connection.close();
        return true;
    }


//    public long insertNewBook(Book book) throws SQLiteException {
//        SQLiteDatabase connection = SQLiteConnector.getWritableDatabase();
//        String sql = "INSERT INTO book VALUES (?,?,?,?,?)";
//        SQLiteStatement statement = connection.compileStatement(sql);
//        statement.bindString(1,book.getBookID());
//        statement.bindString(2,book.getBookName());
//        statement.bindString(3,book.getType());
//        statement.bindString(4,book.getAuthor());
//        statement.bindString(5,book.getImageURL());
//        long rowId = statement.executeInsert();
//        connection.close();
//        return rowId;
//    }

    @Override
    public boolean deleteBook(Book book) throws SQLException {
        SQLiteDatabase db = SQLiteConnector.getWritableDatabase();
        String sqlCommand = "DELETE FROM book WHERE id = ?";
        SQLiteStatement stmt = db.compileStatement(sqlCommand);
        stmt.bindString(1, book.getBookID());
        stmt.executeUpdateDelete();
        return true;
    }

    @Override
    public boolean editBookInfo(Book book) throws SQLException {
        SQLiteDatabase db = SQLiteConnector.getWritableDatabase();
        String sqlCommand = "UPDATE book SET name = ? , type = ? , author = ? , imageurl = ? WHERE id = ?";
        SQLiteStatement stmt = db.compileStatement(sqlCommand);
        stmt.bindString(1, book.getBookName());
        stmt.bindString(2, book.getType());
        stmt.bindString(3, book.getAuthor());
        stmt.bindString(4, book.getImageURL());
        stmt.bindString(5, book.getBookID());
        stmt.executeUpdateDelete();
        return true;
    }

    @Override
    public List<Book> searchBookByName(String name) throws SQLException {
        List<Book> listBooks = new ArrayList<>();
        String selectQuery = "SELECT  * FROM book WHERE name = '" + name + "'";
        SQLiteDatabase db = SQLiteConnector.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Book book = new Book();
                book.setBookID(cursor.getString(0));
                book.setBookName(cursor.getString(1));
                book.setType(cursor.getString(2));
                book.setAuthor(cursor.getString(3));
                book.setImageURL(cursor.getString(4));
                listBooks.add(book);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return listBooks;
    }

    @Override
    public List<Book> searchBookByType(String type) throws SQLException {
        List<Book> listBooks = new ArrayList<>();
        String selectQuery = "SELECT  * FROM book WHERE type = '" + type + "'";
        SQLiteDatabase db = SQLiteConnector.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Book book = new Book();
                book.setBookID(cursor.getString(0));
                book.setBookName(cursor.getString(1));
                book.setType(cursor.getString(2));
                book.setAuthor(cursor.getString(3));
                book.setImageURL(cursor.getString(4));
                listBooks.add(book);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return listBooks;
    }

    @Override
    public List<Book> findAll() throws SQLException {
        List<Book> listBooks = new ArrayList<>();
        String selectQuery = "SELECT  * FROM book";
        SQLiteDatabase db = SQLiteConnector.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Book book = new Book();
                book.setBookID(cursor.getString(0));
                book.setBookName(cursor.getString(1));
                book.setType(cursor.getString(2));
                book.setAuthor(cursor.getString(3));
                book.setImageURL(cursor.getString(4));
                listBooks.add(book);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return listBooks;
    }

    @Override
    public void deleteAll() throws SQLiteException {
        String selectQuery = "DELETE   FROM book";
        SQLiteDatabase db = SQLiteConnector.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
    }
}
