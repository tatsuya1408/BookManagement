package com.bookmgmt.tatsuya.bookmanagement.dao;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import com.bookmgmt.tatsuya.bookmanagement.model.Book;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by tatsuya on 22/03/2018.
 */

public interface BookDAO {
    public abstract boolean createBook(Book book) throws SQLiteException;

    public abstract boolean deleteBook(Book book) throws SQLiteException;

    public abstract boolean editBookInfo(Book book) throws SQLiteException;

    public abstract List<Book> searchBookByName(String name) throws SQLiteException;

    public abstract List<Book> searchBookByType(String type) throws SQLiteException;

    public abstract List<Book> findAll() throws SQLiteException;

    public abstract void deleteAll() throws SQLiteException;
}
