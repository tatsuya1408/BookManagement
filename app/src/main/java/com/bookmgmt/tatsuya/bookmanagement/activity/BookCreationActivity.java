package com.bookmgmt.tatsuya.bookmanagement.activity;

import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bookmgmt.tatsuya.bookmanagement.R;
import com.bookmgmt.tatsuya.bookmanagement.dao.BookDAO;
import com.bookmgmt.tatsuya.bookmanagement.dao.BookDAOImpl;
import com.bookmgmt.tatsuya.bookmanagement.model.Book;
import com.bookmgmt.tatsuya.bookmanagement.utils.SQLiteConnector;

public class BookCreationActivity extends AppCompatActivity {
    private EditText etID;
    private EditText etName;
    private EditText etType;
    private EditText etAuthor;
    private EditText etImageURL;
    private Button btnCreation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_creation);
        mapViewComponents();
        this.btnCreation.setOnClickListener(view -> {

            Book newBook = new Book();
            newBook.setBookID(BookCreationActivity.this.etID.getText().toString());
            newBook.setBookName(BookCreationActivity.this.etName.getText().toString());
            newBook.setType(BookCreationActivity.this.etType.getText().toString());
            newBook.setAuthor(BookCreationActivity.this.etAuthor.getText().toString());
            newBook.setImageURL(BookCreationActivity.this.etImageURL.getText().toString());

            BookDAO bookDAO = new BookDAOImpl(new SQLiteConnector(this));
            try {
                bookDAO.createBook(newBook);
                Intent refresh = new Intent(this, MainActivity.class);
                super.startActivity(refresh);
                super.finish(); //
            } catch (SQLiteException ex) {
                Toast.makeText(this, "can not create new Book", Toast.LENGTH_SHORT);
            }
        });
    }

    private void mapViewComponents() {
        this.etID = super.findViewById(R.id.etIDBookCrt);
        this.etName = super.findViewById(R.id.etNameCrt);
        this.etType = super.findViewById(R.id.etTypeCrt);
        this.etAuthor = super.findViewById(R.id.etAuthorCrt);
        this.etImageURL = super.findViewById(R.id.etImageURLCrt);
        this.btnCreation = super.findViewById(R.id.btnCreation);
    }
}
