package com.bookmgmt.tatsuya.bookmanagement.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bookmgmt.tatsuya.bookmanagement.R;
import com.bookmgmt.tatsuya.bookmanagement.activity.EditBookInfoActivity;
import com.bookmgmt.tatsuya.bookmanagement.activity.MainActivity;
import com.bookmgmt.tatsuya.bookmanagement.dao.BookDAO;
import com.bookmgmt.tatsuya.bookmanagement.dao.BookDAOImpl;
import com.bookmgmt.tatsuya.bookmanagement.model.Book;
import com.bookmgmt.tatsuya.bookmanagement.task.ImageRenderOnline;
import com.bookmgmt.tatsuya.bookmanagement.utils.SQLiteConnector;

import java.util.List;

/**
 * Created by tatsuya on 17/03/2018.
 */

public class BookAdapter extends ArrayAdapter<Book> {
    private Context context;
    private int resource;
    private List<Book> bookList;

    public BookAdapter(@NonNull Context context, int resource, @NonNull List<Book> bookList) {
        super(context, resource, bookList);
        this.context = context;
        this.resource = resource;
        this.bookList = bookList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView = LayoutInflater.from(this.context).inflate(R.layout.activity_book_line, parent, false);
        ImageView imageBook = convertView.findViewById(R.id.imageProduct);
        TextView productName = convertView.findViewById(R.id.productName);
        Button editBookInfoBtn = convertView.findViewById(R.id.editInfoBtn);
        Button deleteBookBtn = convertView.findViewById(R.id.deleteBtn);

        Book book = this.bookList.get(position);
        System.out.println(book.getImageURL());
        new ImageRenderOnline(imageBook).execute(book.getImageURL());
        productName.setText(book.getBookName());
        editBookInfoBtn.setOnClickListener(view -> {
            Intent intent = new Intent(parent.getContext(), EditBookInfoActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("editBook", book);
            intent.putExtra("editBookBundle", bundle);
            parent.getContext().startActivity(intent);
        });

        deleteBookBtn.setOnClickListener(view -> {
            new AlertDialog.Builder(parent.getContext())
                    .setTitle("Confirmation")
                    .setMessage("Are you sure to delete this book?")
                    .setCancelable(false)
                    .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            BookDAO bookDAO = new BookDAOImpl(new SQLiteConnector(parent.getContext()));
                            bookDAO.deleteBook(book);
//                            Intent refresh = new Intent(parent.getContext(), MainActivity.class);
//                            parent.getContext().startActivity(refresh);
                            bookList.remove(position);
                            BookAdapter.this.notifyDataSetChanged(); //Updates adapter to new changes
                        }
                    }).show();

        });
        return convertView;
    }

}
//using code in activity.java
//        this.listViewProduct = super.findViewById(R.id.listViewProduct);
//                BookAdapter foodAdapter
//                = new BookAdapter(this, R.layout.activity_food_line, this.bookList);
//                for (int i = 1; i <= 5; i++) {
//                Food food = new Food(i, "Product " + i, 12000, 100, "");
//                bookList.add(food);
//                }
//                this.listViewProduct.setAdapter(foodAdapter);